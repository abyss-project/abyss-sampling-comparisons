#!/usr/bin/env bash
#abyss preprocess pipeline configuration file

##############################################
#MAIN PARAMETERS
##############################################

#Paths to data and references
DATAREF=TO_BE_SET
#DATAREF=/home/ref-bioinfo-public/ifremer/merlin/abyss/BioinformaticPipelineComparisons
DATAWORK=/home/datawork-merlinabyss/3b_Sampling_comparisons_Blandine
#DATAWORK=/home/datawork-merlinabyss/1_BioinformaticPipelineComparisons/2_operation

#Locus to be analysed (16S-fuhrman or 16S-archaea, COI or 18S-V1 18S-V4 or 18S-V9)
export locus=16S-fuhrman
echo "Barcode locus = $locus"

# OUTPUT DIR WITH RESULTS
export output_dir=$DATAWORK/2_operation/1_raw_MOTU_tables
echo "Output directory for Abyss pipeline = $output_dir"
export ligation_preprocess_dir=${output_dir}/$locus/0_ligation_preprocess
export cutadapt_dir=${output_dir}/${locus}/1_cutadapt
export cleaning_dir=${output_dir}/${locus}/2_R1R2
export dada_dir=${output_dir}/${locus}/4_dada
export blast_dir=${output_dir}/${locus}/6_blast
export bfrogs_dir=${output_dir}/${locus}/7_frogs
export extract_amp_dir=${output_dir}/${locus}/7_extract_amplicons
# processes for final curations after filtration:
export tag_switching_dir=${output_dir}/${locus}/8_tag_switching
export lulu_dir=${output_dir}/${locus}/9_lulu

# INPUT DIR WITH DATA TO PROCESS
export input_dir=$DATAWORK/1_raw_data_dataref/sediment/16S_V4V5_Fuhrman_primers
echo "Input directory with sequencing data = ${input_dir}"

#dada2 merge ASVs tables located in a specific input directory
#Warning : input_merge is the folder containing the ASV tables to merge 
export input_merge=TO_BE_SET
# in case you run dada2mergeasv alone uncomment the export dada_dir
export output_merge=${output_dir}/${locus}/4_dada
#or update it and write:
#export output_merge=$dada_dir

# PIPELINE MAIN DIRECTORY
#Path to abyss-pipeline local git cloned repository
export main_dir=/home/datawork-merlinabyss/abyss-pipeline

#pbs scripts dir
export pbs_dir=$main_dir/pbs
#echo "PBS scripts directory = $pbs_dir"

#R python and sh scripts 
export scripts_dir=$main_dir/commun
#echo "Common scripts directory = $scripts_dir"

# reference databases folder
export ref_dir=$DATAREF/data/sequence-set-nucleic-acid/tools-data/
#echo "Reference databases directory = $ref_dir"

#######################################################
##PIPELINE STEPS TO RUN
#######################################################
#abyss-preprocessing pipeline if input reads need to be preprocessed (cleaning process included)
export ligation_preprocess=no

#cleaning (only if input data are not ligation data)
export cleaning_process=no

#dada2
export dada_main_process=yes
#merge process only : ASV tables to merge are in the folder input_merge, the result will be written in folder output_merge
export dada_mergeasv_process=no

#blast assignation (out of frogs)
export blast_assignation=yes

#frogs
export frogs_process=no
export extract_amplicons=no

# these steps must be run alone
#tag switching normalisation
export tag_switching_norm_process=no

#lulu curation
export lulu_process=no
#######################################################
##LIGATION PROCESS
#######################################################
# input data are ligation-based  (R1F/R2R and R2F/R1R)
export ligation=yes

if [ ${ligation_preprocess} == "yes" ]; then
    export correspondance_file=$DATAWORK/1_raw_data_dataref/sediment/Correspondances_Genoscope_Sediments_SamplingComparisons.csv
    export ligation_config_file=${main_dir}/thirdparty/abyss-preprocessing/extract.ini
    # By default, preprocessing ligation reads (including renaming): set trimreads=True, rename=False
    # If you only want to rename input files with their corresponding abyss sample name without triming reads : set trimreads=False, rename=True
    export trimreads=True
    # rename only step
    export rename=False
fi

#######################################################
#DADA MAIN PROCESS
#######################################################
if [ ${dada_main_process} == "yes" ] || [ ${blast_assignation} == "yes" ]; then
    export script_dada=${scripts_dir}/dada2main.R
    export script_dadamerge=${scripts_dir}/dada2mergeasv.R
    export script_dada2output=${scripts_dir}/dada2outputfiles_80.R
    #echo "Dada2 script path = $script_dada"
    # Parametres Dada : longueurs souhaitees des amplicons
    if [ "$locus" = "COI" ]; then
        #Midori RDP database with/without terrestrial + abyss mock COI sequences
        export rdpbank="$ref_dir/dada2/MIDORI_UNIQUE_20180221_COI_wo_terrestrials_rdp_formatted.fasta"
        #export rdpbank="$ref_dir/dada2/MIDORI_UNIQUE_20180221_COI_rdp_formatted.fasta"
	#Midori BLAST database with/ without terrestrials + abyss mock COI sequences
        export blastbank="$ref_dir/dada2/MIDORI_UNIQUE_20180221_COI_wo_terrestrials.fasta"
	#export blastbank=$ref_dir/frogs/midori/20180221/COI_without_terrestrials/MIDORI_UNIQUE_20180221_COI_wo_terrestrials.fasta
	#export blastbank=$ref_dir/frogs/midori/20180221/COI/MIDORI_UNIQUE_20180221_COI.fasta
	#export blastbank="$ref_dir/dada2/MIDORI_UNIQUE_20180221_COI.fasta"
	#COI Dabatabase extracted from Genbank Core Nucleotide + abyss mock COI sequences
        #export blastbank="$ref_dir/frogs/COI_genbank_2017-12-19-modif-abyss-seq/COI-Genbank_CoreNucleotide_2017-12-19-abyss-mock.fasta"
        #alternative banque MBPK + abyss mock COI sequences 
        #export blastbank=$ref_dir/frogs/COI_MBPK_March2016/db_COI_March2016.fasta
        #alternative with the midori database + abyss mock COI sequences
        #export blastbank=$ref_dir/frogs/midori/20180221/COI/MIDORI_UNIQUE_20180221_COI.fasta
        export lengthMin=300
        export lengthMax=326
    elif [ "$locus" = "16S-fuhrman" ]; then
        export lengthMin=350
        export lengthMax=390
        export blastbank="/home/ref-bioinfo-public/ifremer/merlin/abyss/BioinformaticPipelineComparisons/data/sequence-set-nucleic-acid/tools_data/frogs/16S_silva132/silva_132_16S.fasta"
        export rdpbank="/home/ref-bioinfo-public/ifremer/merlin/abyss/BioinformaticPipelineComparisons/data/sequence-set-nucleic-acid/tools_data/dada2/silva_nr_v132_train_set.fa"
    elif [ "$locus" = "16S-archaea" ]; then
        export lengthMin=350
        export lengthMax=390
        export blastbank="$ref_dir/frogs/16S_silva132/silva_132_16S.fasta"
        export rdpbank="$ref_dir/dada2/silva_nr_v132_train_set.fa"
    elif [ "$locus" = "18S-V1" ]; then
        export lengthMin=330
        export lengthMax=390
        #Silva 18S db + abyss mock 18S sequences
        export blastbank="$ref_dir/frogs/18S_silva132-abyss-seq/silva_132_18S.fasta"
        export rdpbank="$ref_dir/dada2/silva_nr_v132_train_set.fa"
	#export blastbank="$ref_dir/frogs/18S_silva132/silva_132_18S.fasta"
    elif [ "$locus" = "18S-V4" ]; then
        export lengthMin=350
        export lengthMax=410
        #Silva 18S db + abyss mock 18S sequences
        export blastbank="$ref_dir/frogs/18S_silva132-abyss-seq/silva_132_18S.fasta"
        export rdpbank="$ref_dir/dada2/silva_nr_v132_train_set.fa"
	#export blastbank="$ref_dir/frogs/18S_silva132/silva_132_18S.fasta"
    elif [ "$locus" = "18S-V9" ]; then
        export lengthMin=87
        export lengthMax=186
        #Silva 18S db + abyss mock 18S sequences
	#export blastbank="$ref_dir/frogs/18S_silva132/silva_132_18S.fasta"
        #export blastbank="$ref_dir/frogs/18S_silva132-abyss-seq/silva_132_18S.fasta"
        export blastbank=$ref_dir/frogs/mix_18S16S_silva132/mix_18S16S_silva132.fasta
	export rdpbank="$ref_dir/dada2/silva_nr_v132_train_set.fa"
    fi
fi

#######################################################
#DADA MERGE ASV
#######################################################
if [ ${dada_mergeasv_process} == "yes" ]; then
    export script_dadamerge=${scripts_dir}/dada2mergeasv.R
    #echo "Dada2 merge asv tables script path = $script_dadamerge"
fi

#######################################################
#TAG SWITCHING NORMALIZATION
#######################################################
if [ ${tag_switching_norm_process} == "yes" ]; then
    export script_owi_renormalize=${scripts_dir}/owi_renormalize.R
    export tag_switching_cutoff=0.97
    #Path to the tsv count table to normalize (probably : refined_MOTU_tables/10_ASVs_counts_refined.tsv)
    export input_table_tag_switching=TO_BE_SET
    #Colstart is the first column of the tsv table with count data (ie. first sample) and colend is the last column containing count data (ie : the last sample)
    export colstart=TO_BE_SET #col1 = clusterID, is supposed to begin at 2
    export colend=TO_BE_SET #is supposed to be ncol or(sample number)+1
fi

#######################################################
#LULU curation
#######################################################
if [ ${lulu_process} == "yes" ]; then
    export script_lulu=${scripts_dir}/lulu.R
    # Parametres Lulu : identity threshold
    export identite_lulu=84
    # Parametres Lulu : cooccurence threshold
    export cooccurence_lulu=0.90
    #Path to the renormalised count tsv table to curate, OTUs in rows with first column being ClusterID (probably : renormalised_MOTU_table/ASVs_counts_renormalised.tsv)
    export input_table_lulu=TO_BE_SET
    #Path to the fasta of ClusterIDs to curate (probably : LULU_curation_input/10_ASVs_refined.fasta)
    export input_fasta_lulu=TO_BE_SET
    #Path to the taxonomy table to merge in the end (probably : LULU_curation_input/10_ASVs_taxonomy_refined.fasta)
    export input_taxonomy_lulu=TO_BE_SET
fi

#######################################################
#FROGS
#######################################################
if [ ${frogs_process} == "yes" ]; then
    
    #define frogs' clustering method (swarm)
    export clustering=swarm
    if [ ${extract_amplicons} = "yes" ]; then
     	export script_ext_amp=${scripts_dir}/extract_amplicons.sh
    fi
    if [ ${locus} = "16S-fuhrman" ]; then
        # Banque de reference pour l'assignation taxonomique
        export banque=$ref_dir/16S_silva132/silva_132_16S.fasta
        export lengthMin=350
        export lengthMax=390
        export R1size=TO_BE_SET
        export R2size=TO_BE_SET
        export expectedAmpliconSize=TO_BE_SET
        export range=1 #$(seq 1 7)
    elif [ ${locus} = "16S-archaea" ]; then
        # Banque de reference pour l'assignation taxonomique
        export banque=$ref_dir/16S_silva132/silva_132_16S.fasta
        export lengthMin=350
        export lengthMax=390
        export R1size=TO_BE_SET
        export R2size=TO_BE_SET
        export expectedAmpliconSize=TO_BE_SET
        export range=1 #$(seq 1 7)
    elif [ ${locus} = "18S-V1" ]; then
        # Silva
        #export banque=$ref_dir/frogs/18S_silva132/silva_132_18S.fasta
        # Silva with abyssal barcodes from mocks
        export banque=$ref_dir/frogs/18S_silva132-abyss-seq/silva_132_18S.fasta
        # marine protists PR2 Roscoff
        #export banque=$ref_dir/pr2-4.11.0/pr2_version_4.11.0_FROGS.fasta
        export lengthMin=330
        export lengthMax=390
        export R1size=TO_BE_SET
        export R2size=TO_BE_SET
        export expectedAmpliconSize=TO_BE_SET
        export range=4
    elif [ ${locus} = "18S-V4" ]; then
        # Silva
        export banque=$ref_dir/frogs/18S_silva132/silva_132_18S.fasta
        # marine protists PR2 Roscoff
        #export banque=$ref_dir/pr2-4.11.0/pr2_version_4.11.0_FROGS.fasta
        export lengthMin=350
        export lengthMax=410
        export R1size=TO_BE_SET
        export R2size=TO_BE_SET
        export expectedAmpliconSize=TO_BE_SET
        export range=4
    elif [ ${locus} = "18S-V9" ]; then
        # Silva
        #export banque=$ref_dir/frogs/18S_silva132/silva_132_18S.fasta
        # marine protists PR2 Roscoff
        #export banque=$ref_dir/frogs/pr2-4.11.0/pr2_version_4.11.0_FROGS.fasta
        # alternative mix 16S/18S de silva132
        export banque=$ref_dir/mix_18S16S_silva132/mix_18S16S_silva132.fasta
        export lengthMin=87
        export lengthMax=186
        export R1size=TO_BE_SET
        export R2size=TO_BE_SET
        export expectedAmpliconSize=TO_BE_SET
        export range=2
    elif [ ${locus} = "COI" ]; then
        #the midori database with barcodes from mock species:
        #export banque=$ref_dir/frogs/midori/20180221/COI/MIDORI_UNIQUE_20180221_COI.fasta
        #the midori marine-only database with barcodes from mock species:
        export banque=$ref_dir/frogs/midori/20180221/COI_without_terrestrials/MIDORI_UNIQUE_20180221_COI_wo_terrestrials
	#Genbank COI extraction
        #export banque=$ref_dir/frogs/COI_genbank_2017-12-19-modif/COI-Genbank_CoreNucleotide_2017-12-19.fasta
        #Genbank COI extraction with abyssal mock species barcodes
        #export banque=$ref_dir/frogs/COI_genbank_2017-12-19-modif-abyss-seq/COI-Genbank_CoreNucleotide_2017-12-19.fasta
	#Metabarpark database banque MBPK
        #export banque=$ref_dir/COI_MBPK_March2016/db_COI_March2016.fasta
        export lengthMin=300
        export lengthMax=326
        export R1size=TO_BE_SET
        export R2size=TO_BE_SET
        export expectedAmpliconSize=TO_BE_SET
        export range=6
    fi
fi

#######################################################
#Double PCR data preprocess
#######################################################
if [ "$ligation" = "no" ] ; then
    # Sample names is like : Sample_name.[R1format].fastq.gz
    # R1format and R2format has to be set below : 
    export R1format="R1" #stands for Sample_name.R1.fastq.gz
    export R2format="R2"
    #Cutadapt parameters according to locus
    if [ "$locus" = "18S-V1" ]; then
        
        # Parametres Cutadapt : primers, mismatchs autorises (on se debrouille pour que le taux donne 3 nt, sauf pour le COI ou on garde 0.3), et "overlap" qui vaut la longueur du primer moins 1
        export primerF="-g GCTTGTCTCAAAGATTAAGCC"
        export primerR="-g CCTGCTGCCTTCCTTRGA"
        export error_rateF=0.15
        export error_rateR=0.17
        export overlapF=20
        export overlapR=17
        
    elif [ "$locus" = "18S-V4" ]; then
        
        # Parametres Cutadapt : primers, mismatchs autorises (on se debrouille pour que le taux donne 3 nt, sauf pour le COI ou on garde 0.3), et "overlap" qui vaut la longueur du primer moins 1
        export primerF="-g CCAGCASCYGCGGTAATTCC"
        export primerR="-g ACTTTCGTTCTTGATYRA"
        export error_rateF=0.15
        export error_rateR=0.17
        export overlapF=20
        export overlapR=17
        
    elif [ "$locus" = "18S-V9" ]; then
        
        # Parametres Cutadapt : primers, mismatchs autorises (on se debrouille pour que le taux donne 3 nt, sauf pour le COI ou on garde 0.3), et "overlap" qui vaut la longueur du primer moins 1
        export primerF="-g TTGTACACACCGCCC"
        export primerR="-g CCTTCYGCAGGTTCACCTAC"
        export error_rateF=0.15
        export error_rateR=0.17
        export overlapF=20
        export overlapR=17
        
    elif [ "$locus" = "16S-fuhrman" ]; then
        	
       	# Parametres Cutadapt : primers, mismatchs autorises (on se debrouille pour que le taux donne 3 nt, sauf pour le COI ou on garde 0.3), et "overlap" qui vaut la longueur du primer moins 1
        export primerF="-g GTGYCAGCMGCCGCGGTAA"
        export primerR="-g CCGYCAATTYMTTTRAGTTT"
        export error_rateF=0.17
        export error_rateR=0.15
        export overlapF=18
        export overlapR=19
        
    elif [ "$locus" = "16S-archaea" ]; then
        	
       	# Parametres Cutadapt : primers, mismatchs autorises (on se debrouille pour que le taux donne 3 nt, sauf pour le COI ou on garde 0.3), et "overlap" qui vaut la longueur du primer moins 1
        export primerF="-g GCCTAAAGCATCCGTAGC -g GCCTAAARCGTYCGTAGC -g GTCTAAAGGGTCYGTAGC -g GCTTAAAGNGTYCGTAGC -g GTCTAAARCGYYCGTAGC"
        export primerR="-g CCGGCGTTGANTCCAATT"
        export error_rateF=0.17
        export error_rateR=0.15
        export overlapF=18
        export overlapR=19
        
    else
	#COI
       	# Parametres Cutadapt : primers, mismatchs autorises (on se debrouille pour que le taux donne 3 nt, sauf pour le COI ou on garde 0.3), et "overlap" qui vaut la longueur du primer moins 1
        export primerF="-g GGWACWGGWTGAACWGTWTAYCCYCC"
        export primerR="-g TANACYTCNGGRTGNCCRAARAAYCA"
        export error_rateF=0.27
        export error_rateR=0.27
        export overlapF=25
        export overlapR=25
    fi
fi
