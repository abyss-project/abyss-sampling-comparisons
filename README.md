# abyss-sampling-comparisons

config files and R scripts for the sampling comparisons manuscript

This material contains information to process data from:

ftp://ftp.ifremer.fr/ifremer/dataref/bioinfo/merlin/abyss/abyss-sampling-comparisons/

using the abyss-pipeline tool